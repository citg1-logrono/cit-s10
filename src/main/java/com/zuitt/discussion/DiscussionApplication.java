package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	@GetMapping("/users")
	public String getAllUser(){
		return "All user retrieved";
	}

	@PostMapping("/users")
	public String createUser(){
		return "New user created.";
	}

	@GetMapping("/users/{id}")
	public String getUser(@PathVariable Long id){
		return "Hello "+id;
	}

	@DeleteMapping("/users/{id}")
	public ResponseEntity<?> deleteUser(@PathVariable Long id, @RequestHeader("Authorization") String user) {
		if (user != null && !user.isEmpty()) {

			return ResponseEntity.ok().body("The User " + id + " has been deleted.");
		} else {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Unauthorized access.");
		}
	}

	@PutMapping("/users/{id}")
	@ResponseBody
	public Users updateName(@PathVariable Long id, @RequestBody Users user) {

		return user;
	}
}
